#include "stdio.h"


int main(void) {
    long intVar1;
    int intVar2;
    int bitValue;

    printf("Enter an integer: ");
    scanf("%ld", &intVar1);
    switch (intVar1) {
    case 100 ... 1000:
        printf("1. 100 <= %ld <= 1000\n", intVar1);
        break;
    default:
        printf("1. число %ld не попадает в диапазон 100 ... 1000\n", intVar1);
        break;
    }

    printf("Enter an integer: ");
    scanf("%d", &intVar2);
    bitValue = (intVar2 >> 22) & 1;
    printf("2. 22nd bit value is %d\n", bitValue);
    return 0;
}
