#include "stdio.h"


void print2x2Matrix(int matrix[2][2]) {
    for (int i = 0; i < 2; i++) {
        printf("[");
        for (int j = 0; j < 2; j++) {
            if (j != 1) {
                printf("%d\t", matrix[i][j]);
            } else {
                printf("%d", matrix[i][j]);
            }
        }
        printf("]");
        printf("\n");
    }
}


int main(void) {
    int arr[] = {23, 5678, 2, 564, 365, 77, 443};
    int arrLen = sizeof(arr) / sizeof(arr[0]);
    int matrixA[2][2] = {{1,2}, {-3,4}}, matrixB[2][2] = {{-2,4}, {3,1}};
    int resultMatrix[2][2];

    printf("%s\n", "1)");
    for (int i = 0; i < arrLen; i++) {
        printf("Array[%d] = %d\n", i, arr[i]);
    }
    printf("\n");
    
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            int sum = 0;
            for (int k = 0; k < 2; k++) {
                sum += matrixA[i][k] * matrixB[k][j];
            }
            resultMatrix[i][j] = sum;
        }
    }
    
    printf("%s\n", "2)");
    print2x2Matrix(resultMatrix);

    // доп. задание:
    int transposedMatrix[2][2] = {{0, 0}, {0, 0}};
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            transposedMatrix[i][j] = resultMatrix[j][i];
        }
    }

    printf("%s\n", "3)");
    print2x2Matrix(transposedMatrix);

    return 0;
}
