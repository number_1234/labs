#include "stdio.h"


enum month {
    January = 1,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};

int main(void) {
    printf("1)\n");
    enum month monthName = July;
    printf("%d\n", monthName);
    return 0;
}
