#include <stdio.h>
#include <math.h>


struct point {
    int x;
    int y;
};

struct point makePoint(int x, int y) {
    struct point result;
    result.x = x;
    result.y = y;
    return result;
};

struct rectangle {
    struct point pt1, pt2, pt3, pt4;
    int p;
};

int calcP(struct rectangle figure) {
    /* рассчёт периметра фигуры */
    int p, v1, v2, v3, v4;
    v1 = sqrtf((figure.pt2.x - figure.pt1.x) ^ 2 + (figure.pt2.y - figure.pt1.y) ^ 2);
    v2 = sqrtf((figure.pt3.x - figure.pt2.x) ^ 2 + (figure.pt3.y - figure.pt2.y) ^ 2);
    v3 = sqrtf((figure.pt4.x - figure.pt3.x) ^ 2 + (figure.pt4.y - figure.pt3.y) ^ 2);
    v4 = sqrtf((figure.pt1.x - figure.pt4.x) ^ 2 + (figure.pt1.y - figure.pt4.y) ^ 2);
    p = v1 + v2 + v3 + v4;
    return p;
};

void run() {
    struct rectangle rect;
    rect.pt1.x = 1;
    rect.pt1.y = 1;
    rect.pt2.x = 5;
    rect.pt2.y = 5;
    rect.pt3.x = 9;
    rect.pt3.y = 9;
    rect.pt4.x = 13;
    rect.pt4.y = 13;
    rect.p = calcP(rect);
    printf("P = %d\n", rect.p);
};

int main(void) {
    run();
    return 0;
}
