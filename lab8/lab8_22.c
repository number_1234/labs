#include "stdio.h"
#include "string.h"


int main(void) {
    char string1[10], string2[10];
    int n;

    printf("enter first string (not more than 10 chars):\n");
    scanf("%s", &string1);
    printf("enter second string (not more than 10 chars):\n");
    scanf("%s", &string2);
    printf("2) n: \n");
    scanf("%d", &n);
    int len1 = strlen(string1);
    int tmp = len1; // idx in string1
    for (int i = 0; i <= n; i++) {
        string1[tmp] = string2[i];
        tmp++;
    }
    printf("%s\n", string1);

    strcpy(string1, string2); // copy "string2" into "string1"
    printf("5) copied string2 into string1: %s\n", string1);

    char string3[15], *p;
    char sym;
    printf("8) enter some string3 (not more than 15 chars):\n");
    scanf("%s", &string3);
    printf("enter a char to search in the string3:\n");
    fflush(stdin);
    scanf("%s", &sym); // char to find
    p = strchr(string3, sym); // first entry
    printf("string3[%d]\n", p-string3);

    printf("9) enter a char to search in the string3:\n");
    fflush(stdin);
    scanf("%s", &sym);
    p = strrchr(string3, sym); // last entry
    printf("string3[%d]\n", p-string3);

    //12
    char s1[10], s2[10];
    printf("12) enter string1 (not more than 10 chars):\n");
    scanf("%s", s1);
    printf("enter string2 (not more than 10 chars):\n");
    scanf("%s", s2);

    int strLen = sizeof(string1) / sizeof(char);
    // will add later
    return 0;
}
