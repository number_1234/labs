#include "stdio.h"


int main () {
    char stringVar[50];
    long intVar;

    printf("Enter some string: ");
    scanf("%[^\n]s", stringVar);
    printf("Enter an integer: ");
    scanf("%d", &intVar);
    printf("Your string: %s\n", stringVar);
    printf("Your integer: %d\n", intVar);
    return 0;
}
