#include "stdio.h"


int main(void) {
    int number1, number2;

    printf("1. Enter oct number1: ");
    scanf("%o", &number1);
    printf("2. Dec number1 is %d\n", number1);
    printf("3. Hex number1 is %X\nnumber1 >> 2 = %d\n", number1, number1 >> 2);
    printf("4. Hex number1 is %X\n ~number1 = %X\n", number1, ~number1);
    printf("5. Enter hex number2: ");
    scanf("%X", &number2);
    printf("Hex number1 | number2 = %X\n", number1 | number2);
    return 0;
}
