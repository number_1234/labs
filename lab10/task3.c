#include "stdio.h"
#include "string.h"
#include "stdbool.h"


bool valueInArray(int arrLen, int arr[arrLen], int value) {
    for (int i = 0; i < arrLen; i++) {
        if (arr[i] == value) {
            return true;
        }
    }
    return false;
}

int main(void) {
    char input[6];

    printf("enter a number <= 999999:\n");
    scanf("%s", (char*)input);
    int strLen = strlen(input);
    
    int numbers[strLen];
    for (int i = 0; i < strLen; i++) {
        if (!(valueInArray(strLen, numbers, (int)input[i]))) {
            numbers[i] = (int)input[i];
            printf("%c\n", (int)input[i]);
        }
    }

    for (int i = 0; i < 10; i++) {
        switch (numbers[i]) {
        case 0 ... 9:
            printf("%d\n", numbers[i]);
            break;
        default:
            return 0;
        }
    }
}