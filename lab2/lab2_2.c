#include "stdio.h"
#include "math.h"
#include "stdlib.h"


float z1(double alphaRad) {
    return cos(alphaRad) + sin(alphaRad) + cos(3*alphaRad) + sin(3*alphaRad);
}

float z2(double alphaRad) {
    return 2 * sqrt(2) * cos(alphaRad) * sin(M_PI/4 + 2*alphaRad);
}

void additionalTask(void) {
    printf("\n");
    printf("sqrt(1024) = %lf\n", sqrt(1024));
    printf("(-7)^3 = %lf\n", pow(-7, 3));
    printf("|(-123)| = %d\n", abs(-123));
}


int main(void) {
    double alphaDeg, alphaRad;

    printf("Enter alpha value (degrees): ");
    scanf("%lf", &alphaDeg);
    alphaRad = alphaDeg * (M_PI / 180); // convert 'alpha' to radians
    printf("z1(alpha) = %lf\n", z1(alphaRad));
    printf("z2(alpha) = %lf\n", z2(alphaRad));
    additionalTask(); // запуск доп. задания
    return 0;
}
